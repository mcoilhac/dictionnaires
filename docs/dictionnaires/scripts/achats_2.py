def calcul_prix(produits, catalogue):
    ...

assert calcul_prix({"brocoli":2, "mouchoirs":5, "bouteilles d'eau":6},
                    {"brocoli":1.50, "bouteilles d'eau":1, "bière":2,
                    "savon":2.50, "mouchoirs":0.80}) == 13.0

produits = {'pain': 1, 'déodorant': 1, 'café': 1, 'vin': 1, 'farine': 1, 'yaourts': 2,
            'jus de fruits': 1, 'pâtes': 1, 'jambon': 1, 'savon': 1, 'confiture': 1,
            'petits gâteaux': 3, 'frites': 1, 'fromage': 1, 'poisson': 2}
catalogue = {'chocolats': 3.2, 'déodorant': 2.2, 'vin': 6.3, 'tomate': 0.75,
            'confiture': 3.15, 'jambon': 2.1, 'savon': 1.9, "bouteilles d'eau": 1,
            'jus de fruits': 2.25, 'frites': 3.55, 'sucre': 0.65, 'dentifrice': 1.95,
            'brocoli': 1.5, 'pack de fruits': 3.3, 'pain': 1.25, 'café': 4.75,
            'yaourts': 2.85, 'citron': 0.9, 'pack de légumes': 4.2, 'huile': 1.65,
            'poisson': 6.45, 'viande': 5.2, 'shampooing': 2.5, 'petits gâteaux': 4.35,
            'bière': 2, 'mouchoirs': 0.8, 'pâtes': 1.1, 'farine': 0.95, 'riz': 3.1,
            'fromage': 2.65}

print( calcul_prix(produits, catalogue))

# Remarque : le résultat obtenu est parfois 63.79999999999999
# On déconseille de tester l'égalite de deux flottants

