ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}

ferme_gaston.pop("cochon")
print(ferme_gaston)

supprime = ferme_gaston.pop("lapin") # La méthode pop renvoie la valeur supprimée.
                                     # On peut affecter la valeur supprimée à une variable 
print(ferme_gaston)
print(supprime)
