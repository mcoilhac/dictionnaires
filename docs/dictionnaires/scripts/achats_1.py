def facture(achats, prix) -> int:
    ...


mes_achats = {"farine": 2, "beurre": 1, "sucre": 2}
mes_prix = {"farine": 1, "sucre": 3, "lait": 2, "beurre": 2}

# Tests
assert facture(mes_achats, mes_prix) == 10
